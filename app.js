/**
 * Need a simple way to look at a user's badge count and JavaScript points
 * Use Node.js to connect to Treehouse's API to get profile information to print
 * Connect to the API URL: http://teamtreehouse.com/username.json
 * -Read the data
 * -Parse the data
 * -Print the data
 */
var profile = require("./profile");

var users = process.argv.slice(2);

users.forEach(profile.get);


